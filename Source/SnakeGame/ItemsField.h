// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Food.h"
#include "Desease.h"
#include "Accelerator.h"
#include "SnakeBase.h"
#include "ItemsField.generated.h"


UENUM()
enum class ItemType
{
	Food,
	Desease,
	Accelerator,
	Total
};

UCLASS()
class SNAKEGAME_API AItemsField : public AActor
{
	GENERATED_BODY()

	static const int FieldSize = 16;
	bool Items[FieldSize][FieldSize]{ false };
	float ElementSize = 60.f;
	FVector ItemsFieldLocation;

	// Gets random int in range, inclusively
	int GetRandomNumber(int min, int max);

	// Finds an empty cell of items field and reserves it for item generation
	void FindAndReserveEmptyCell(int& x, int& y);

	// Gets item location in empty cell of items field
	UFUNCTION()
		FVector GetItemLocation(int x, int y);

	// Gets random item type of ItemType enum
	UFUNCTION()
		ItemType GetRandomItemType();

public:	
	// Sets default values for this actor's properties
	AItemsField();
	
	UPROPERTY(EditDefaultsOnly, Category = "Bonus")
		TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly, Category = "Bonus")
		TSubclassOf<ADesease> DeseaseClass;

	UPROPERTY(EditDefaultsOnly, Category = "Bonus")
		TSubclassOf<AAccelerator> AcceleratorClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Generates random item in random empty cell of items field
	// Should be used when item is eaten by snake
	UFUNCTION()
		void GenerateItem();

	void GenerateSelectedItem(int x, int y);
};
