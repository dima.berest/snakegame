// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameGameModeBase.h"

// Called when the game starts or when spawned
void ASnakeGameGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	AItemsField* Field = (AItemsField*)GetWorld()->SpawnActor(ItemsFieldClass, &ItemsFieldTransform);
	
	if (IsValid(Field))
	{
		Field->GenerateItem();
	}
}