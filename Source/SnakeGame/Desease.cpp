// Fill out your copyright notice in the Description page of Project Settings.


#include "Desease.h"
#include "SnakeBase.h"

// Sets default values
ADesease::ADesease()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ADesease::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ADesease::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ADesease::Interact(AActor* Interactor, bool bIsHead)
{
	if (!bIsHead)
	{
		return;
	}
	auto Snake = Cast<ASnakeBase>(Interactor);	// get snake pointer by Cast
	if (IsValid(Snake))
	{
		Snake->RemoveSnakeElement();	// add a snake element (= 1 by def)
		if (OnItemConsumed.IsBound())
		{
			OnItemConsumed.Execute();
		}
		Destroy();
	}
}