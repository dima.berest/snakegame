// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (!bIsHead)
	{
		return;
	}
	auto Snake = Cast<ASnakeBase>(Interactor);	// get snake pointer by Cast
	if (IsValid(Snake))
	{
		Snake->AddSnakeElement();	// add a snake element (= 1 by def)
		if (OnItemConsumed.IsBound())
		{
			OnItemConsumed.Execute();
		}
		Destroy();
	}
}

