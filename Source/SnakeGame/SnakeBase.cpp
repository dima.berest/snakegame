// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "SnakeGameGameModeBase.h"
#include "Kismet/GameplayStatics.h"	// for UGameplayStatics
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.5f;
	SpeedUpperLimit = 0.15f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);	// change tick frequency (seconds for 1 snake move)
	CreateSnake();

	// write snake ref to GameMode
	ASnakeGameGameModeBase* GameMode = Cast<ASnakeGameGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(GameMode))
	{
		GameMode->Snake = this;
	}
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement()
{
	FVector NewLocation(SnakeElements.Last()->GetActorLocation());
	AddSnakeElementByLocation(NewLocation);
}

void ASnakeBase::RemoveSnakeElement()
{
	if (SnakeElements.Num() > StartSnakeLength)
	{
		auto LastElement = SnakeElements.Last();
		SnakeElements.Remove(LastElement);
		LastElement->Destroy();
	}
}

void ASnakeBase::AddSnakeElementByLocation(FVector Location)
{
	FTransform NewTransform(Location);		// set transform
	ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);	// made new element
	NewSnakeElem->SnakeOwner = this;		// pointer to snake
	int32 ElementIndex = SnakeElements.Add(NewSnakeElem);	// added element to array and save it's index
	if (ElementIndex == 0)
	{
		NewSnakeElem->SetFirstElementType();
	}
}

void ASnakeBase::CreateSnake()
{
	for (int i = 0; i < StartSnakeLength; i++)
	{
		FVector NewLocation(i * ElementSize, 0, 0);
		AddSnakeElementByLocation(NewLocation);
	}	
}

void ASnakeBase::Move()
{
	// making movement vector
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);	// SetActorLocation(GetActorLocation() + MovementVector)
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if(IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)		// if not nullptr pointer, then actor-overlapper implements Interactable (interface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::IncreaseSpeed()
{
	if (MovementSpeed > SpeedUpperLimit)
		SetActorTickInterval(MovementSpeed *= 0.95);
}