// Fill out your copyright notice in the Description page of Project Settings.


#include "Accelerator.h"
#include "SnakeBase.h"

// Sets default values
AAccelerator::AAccelerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAccelerator::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAccelerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAccelerator::Interact(AActor* Interactor, bool bIsHead)
{
	if (!bIsHead)
	{
		return;
	}
	auto Snake = Cast<ASnakeBase>(Interactor);	// get snake pointer by Cast
	if (IsValid(Snake))
	{
		Snake->IncreaseSpeed();	// Increase snake speed
		if (OnItemConsumed.IsBound())
		{
			OnItemConsumed.Execute();
		}
		Destroy();
	}
}