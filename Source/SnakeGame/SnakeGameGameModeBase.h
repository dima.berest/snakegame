// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SnakeBase.h"
#include "ItemsField.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditDefaultsOnly, Category = "BonusGenerator")
	TSubclassOf<AItemsField> ItemsFieldClass;
	
	FVector ItemsFieldLocation = FVector(-480.0, -480.0, 0.0);
	FRotator ItemsFieldRotation = FRotator( 0.0, 0.0, 0.0 );
	FTransform ItemsFieldTransform = FTransform(ItemsFieldRotation, ItemsFieldLocation);

	ASnakeBase* Snake;
};