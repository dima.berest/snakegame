// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ItemsField.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
		float MovementSpeed;
	float SpeedUpperLimit;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY()
		int StartSnakeLength = 4;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement();
	
	// Removes 1 snake element if number of elements > StartSnakeLength
	UFUNCTION(BlueprintCallable)
		void RemoveSnakeElement();

	UFUNCTION(BlueprintCallable)
		void Move();

	// Increases snake speed until upper limit
	UFUNCTION()
		void IncreaseSpeed();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);		// which SnakeElement was overlapped (OverlappedActor) and what overlapped it (Other) 

private:
	void AddSnakeElementByLocation(FVector Location);
	void CreateSnake();
};
