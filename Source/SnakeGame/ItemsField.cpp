// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemsField.h"
#include "SnakeGameGameModeBase.h"
#include "Kismet/GameplayStatics.h"	// for UGameplayStatics
#include <cstdlib>	// for rand() and srand()
#include <ctime>	// for time()
#include <Runtime/Engine/Classes/GameFramework/NavMovementComponent.h>

// Sets default values
AItemsField::AItemsField()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	srand(static_cast<unsigned int>(time(0)));
	rand();
}

// Called when the game starts or when spawned
void AItemsField::BeginPlay()
{
	Super::BeginPlay();

	// get ItemsField coords from GameMode
	ASnakeGameGameModeBase* GameMode = Cast<ASnakeGameGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(GameMode))
	{
		ItemsFieldLocation = GameMode->ItemsFieldLocation;
	}
}

// Called every frame
void AItemsField::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

int AItemsField::GetRandomNumber(int min, int max)
{
	static const double Fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);

	// Distribute a random number in the range evenly:
	return static_cast<int>(rand() * Fraction * (max - min + 1) + min);
}

void AItemsField::FindAndReserveEmptyCell(int& x, int& y)
{
	// gives 10 attempts to find an empty cell
	for (int Attempts = 0; Attempts < 10; ++Attempts)
	{
		x = GetRandomNumber(0, FieldSize - 1);
		y = GetRandomNumber(0, FieldSize - 1);
		if (Items[x][y] == false)
		{
			Items[x][y] = true;
			return;
		}
	}
}

FVector AItemsField::GetItemLocation(int x, int y)
{
	FVector Shift(x * ElementSize, y * ElementSize, 0);
	return ItemsFieldLocation + Shift;
}

ItemType AItemsField::GetRandomItemType()
{
	ItemType RandomItem = static_cast<ItemType>(GetRandomNumber(0, static_cast<int>(ItemType::Total) - 1));
	return RandomItem;
}

void AItemsField::GenerateSelectedItem(int x, int y)
{
	FVector ItemLocation = GetItemLocation(x, y);
	FTransform ItemTransform(ItemLocation);
	FActorSpawnParameters params;
	auto* Food = GetWorld()->SpawnActor<AFood>(FoodClass, ItemTransform, params);
}

void AItemsField::GenerateItem()
{
	int x = 0;
	int y = 0;

	FindAndReserveEmptyCell(x, y);
	auto RandomItemType = GetRandomItemType();
	FVector ItemLocation = GetItemLocation(x, y);
	FTransform ItemTransform(ItemLocation);
	FActorSpawnParameters params;
	params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;

	if (RandomItemType == ItemType::Food)
	{
		auto* Food = GetWorld()->SpawnActor<AFood>(FoodClass, ItemTransform, params);
		Food->OnItemConsumed.BindLambda([this]
		{
			GenerateItem();
		});
	}
	else if (RandomItemType == ItemType::Desease)
	{
		auto* Desease = GetWorld()->SpawnActor<ADesease>(DeseaseClass, ItemTransform, params);
		Desease->OnItemConsumed.BindLambda([this]
		{
			GenerateItem();
		});
	}
	else if (RandomItemType == ItemType::Accelerator)
	{
		auto* Accelerator = GetWorld()->SpawnActor<AAccelerator>(AcceleratorClass, ItemTransform, params);
		Accelerator->OnItemConsumed.BindLambda([this]
		{
			GenerateItem();
		});
	}
}